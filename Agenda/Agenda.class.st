Class {
	#name : #Agenda,
	#superclass : #Object,
	#instVars : [
		'totalContactos',
		'contactos'
	],
	#category : #Agenda
}

{ #category : #'user-interface' }
Agenda >> buscar:texto [

| conts res |
"validations"
(texto isString) ifFalse: [ ^'error, debe ser un string' ].
conts:= self contactos.
(conts isNil) ifTrue: [ ^'error, no existen contactos' ].

"buscar coincidencias"
res:= conts select: [ :el | 
	((el nombre) asLowercase = texto) or: (
		((el apellido) asLowercase = texto) or: (el telefono = texto)
	). 
].
(res isEmpty) ifTrue: [ ^'sin resultados' ].

Transcript clear.
self msjSimple: ('Se encontraron {1} resultado{2}' format: {
	res size.
	(res size ~= 1) ifTrue: [ 's' ] ifFalse: [ '' ].
}).
self salto.
res do: [ :el | 
	self msjSimple: ('Nombre y apellido: "{1} {2}".' format: { el nombre. el apellido }). 
	self salto.
	self msjSimple: ('ID: #{1}' format: { el idContacto }). self salto. 
	self msjSimple: '-----------'. self salto.
].




]

{ #category : #accessing }
Agenda >> contactos [

"Transcript clear.
Transcript show: '-- MOSTRANDO AGENDA --'.
Transcript show: 'Total de contactos:', self totalContactos."

^contactos 
]

{ #category : #accessing }
Agenda >> contactos:nuevo [

((nuevo class) = Contacto) ifFalse: [ ^'error' ].

(contactos isNil) ifTrue: [ contactos:= OrderedCollection new. ].

contactos add: nuevo.

totalContactos:= contactos size.
]

{ #category : #accessing }
Agenda >> eliminar:contactoId [

| conts res |
(contactoId isString) ifFalse: [ ^'error, debe ser alphanumeric' ].
conts := self contactos.
(conts isNil) ifTrue: [ ^'error, lista vacia' ].

res:= conts select: [ :el |
	el idContacto = contactoId.
].

(res isEmpty) ifTrue: [ ^'error, no se encontro ese ID' ].

conts remove: (res at: 1).

^'se borro correctamente'.
]

{ #category : #'user-interface' }
Agenda >> load: pathOfFile [
	"debe pasar la ruta completa del archivo"

| dirTrabajo pathF pArchi contenido conAct add id nombre apellido tel |

(pathOfFile isString) ifFalse: [ ^'error, debe ser un string' ].
dirTrabajo := FileLocator root / pathOfFile.

"crea carpeta si no existe"
dirTrabajo ensureCreateDirectory.
dirTrabajo isDirectory ifFalse: [ ^'error creating folder' ].
""
pathF := (dirTrabajo / 'agenda.txt').
pathF ensureCreateFile.

pArchi := pathF readStream.
contenido := pArchi contents.
pArchi close.

contenido isEmpty ifTrue: [ ^'no hay contenido' ].

contenido := contenido splitOn: '#'.
contenido := contenido select: [ :c | c size > 0 ].

"contactos:= OrderedCollection new."
contenido do: [ :co |
	conAct := co splitOn: '-'.
	conAct := conAct select: [ :ca | ca size > 0 ].
	"contactos add: conAct."
	id := conAct at: 1.
	nombre := conAct at: 2.
	apellido := conAct at: 3.
	tel := conAct at: 4.
	add := Contacto new.
	add := add nombre: nombre apellido: apellido telefono: tel id: id.
	self contactos: add.
].

^'ok'.
]

{ #category : #'user-interface' }
Agenda >> mostrarContactos [

| msjSimple salto num |

salto:= [Transcript cr]. 
msjSimple := [ :msj :sal | 
	Transcript show: ('{1}' format:{ msj }). 
	(sal = 1) ifTrue: [ salto value. ] 
].

Transcript clear.
Transcript show: '-- MOSTRANDO AGENDA --'. Transcript cr.
Transcript show: ('Total de contactos: {1}' format: { self totalContactos }).
salto value. salto value.

((self totalContactos) = 0) ifTrue: [
	msjSimple value: 'No hay contactos...'.
	^1.
].

num := 1.
(self contactos) do: [ :con | 
	msjSimple value: ('Contacto Nro. {1}' format: { num }) value: 1. 
	msjSimple value: ('ID: {1}' format: { con idContacto }) value: 1.
	msjSimple value: ('Nombre y Apellido: {1} {2}' format: { con nombre. con apellido. }) value: 1.
	msjSimple value: ('Telefono: {1}' format: { con telefono }) value: 1.
	msjSimple value: '--------------' value: 1.
	num := num + 1.
].
]

{ #category : #'user-interface' }
Agenda >> msjSimple:msj [
Transcript show: ('{1}' format:{ msj }).
]

{ #category : #'user-interface' }
Agenda >> salto [
Transcript cr.
]

{ #category : #accessing }
Agenda >> totalContactos [
	"comment stating purpose of message"
(totalContactos isNil) ifTrue: [ ^0. ].
^totalContactos
]
