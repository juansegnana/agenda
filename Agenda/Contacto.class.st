Class {
	#name : #Contacto,
	#superclass : #Object,
	#instVars : [
		'nombre',
		'apellido',
		'telefono',
		'idContacto',
		'pais'
	],
	#category : #Agenda
}

{ #category : #accessing }
Contacto >> apellido [
^apellido
]

{ #category : #accessing }
Contacto >> apellido:valor [
apellido:=valor.
]

{ #category : #'as yet unclassified' }
Contacto >> generarId [
	"comment stating purpose of message"
^((000 to: 999) atRandom) asString, (($a to: $z) atRandom asString).
]

{ #category : #accessing }
Contacto >> idContacto [
^idContacto
]

{ #category : #accessing }
Contacto >> nombre [

^nombre.
]

{ #category : #accessing }
Contacto >> nombre:valor [

((valor class) isString) 
ifTrue: [ 
	^'error'.
].
nombre:= valor.
]

{ #category : #'as yet unclassified' }
Contacto >> nombre:name apellido:surname telefono:numbera [

((name isString) and: (surname isString)) ifFalse: [ ^'error' ].
(numbera isAllDigits) ifFalse: [ ^'error' ].

nombre:= name.
apellido:= surname.
telefono:= numbera.
idContacto:= self generarId.



]

{ #category : #'as yet unclassified' }
Contacto >> nombre:name apellido:surname telefono:numbera id:idOr [

((name isString) and: (surname isString)) ifFalse: [ ^'error' ].
(numbera isAllDigits) ifFalse: [ ^'error' ].

nombre:= name.
apellido:= surname.
telefono:= numbera.
idContacto:= idOr.
]

{ #category : #accessing }
Contacto >> pais [
^pais
]

{ #category : #accessing }
Contacto >> telefono [
^telefono
]

{ #category : #accessing }
Contacto >> telefono:numero [
(numero isAllDigits) ifTrue: [ telefono:= numero. ] ifFalse: [ ^'error' ].
]
